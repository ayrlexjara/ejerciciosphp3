<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<style>
	table{
		border-collapse: collapse;	
        margin: 0 auto;
	}
    h3{
		text-align: center;    
    }
</style>
<body>

<?php

	const N = 21;
    
	echo"<table border = '1'></br>";
	echo"<h3>Numeros Pares entre 1 y ".N."</h3>";
	
	for ($i=1; $i <= N; $i++)
	{
    	if($i%2==0){
			echo"<tr>";
			echo"<td>$i</td>";
			echo"</tr>";
        }
	}

?>

</body>
</html>