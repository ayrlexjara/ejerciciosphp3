<?php
	try{
		$conn = new PDO('pgsql:host=localhost;dbname=ejercicio1;','postgres','somormujo95');
			
			$query = "select p.nombre as nombreproducto, p.precio,c.nombre as nombrecategoria,
					m.nombre as nombremarca,e.nombre as nombreempresa
					from producto p
					inner join categoria c using(id_categoria)
					inner join marca m using (id_marca)
					inner join empresa e using (id_empresa);";
			
			$sql = $conn->prepare($query);
			$sql->execute();
			$resultado = $sql->fetchAll();

			$s = "<table><tr><td>Nom. Producto</td><td>Precio</td><td>Marca</td><td>Empresa</td><td>Categoria</td></tr>";
			foreach ($resultado as $row){
				$s .= "<tr><td>". $row['nombreproducto'] ."</td><td>". $row['precio'] ."</td><td>". $row['nombrecategoria'] 
				."</td><td>". $row['nombremarca'] ."</td><td>". $row['nombreempresa'] ."</td></tr>";
			}
			$s .= "</table>";
			echo $s;

			$conn = null; 

		}catch(PDOException $e){
			echo "Error al abrir la BD" . $e->getMessage();
		}
	?>

